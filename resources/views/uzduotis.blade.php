@extends('Layouts.main')

@section('turinys')

    <h1>{{$task->task}}</h1>
    
    <div class="col-sm-6">
        <form action="{{ action('TaskController@update', $task->id) }}" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="date">Pasirinkti kategoriją: </label>
                <select name="category_id">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->category}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="category">Redaguoti užduotį: </label>
                <input type="text" class="form-control" name="task" value="{{$task->task}}">
            </div>
            <div class="form-group">
                <label for="date">Redaguoti terminą: </label>
                <input type="datetime-local" step=1 class="form-control" name="date" value="{{$task_date}}">
            </div>
            <input name="_method" type="hidden" value="PUT">
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <button type="submit" class="btn btn-primary">Išsaugoti</button>
        </form>
    </div>
    
@endsection

