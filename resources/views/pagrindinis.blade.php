@extends('Layouts.main')

@section('turinys')
    
    @if(Session::has("create_task"))
        <div class="alert alert-success" role="alert">
            {{session("create_task")}}
        </div>
    @endif
    
    @if(Session::has("edit_category"))
        <div class="alert alert-success" role="alert">
            {{session("edit_category")}}
        </div>
    @endif
    
    @if(Session::has("delete_task"))
        <div class="alert alert-danger" role="alert">
            {{session("delete_task")}}
        </div>
    @endif

    <h1>Užduočių tvarkyklė</h1>
    
    <div class="col-sm-6">
        <form action="{{ action('TaskController@store') }}" method="post" enctype="multipart/form-data">
           <div class="form-group">
                <label for="date">Pasirinkti kategoriją: </label>
                <select name="category_id">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->category}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="task">Sukurti užduotį: </label>
                <input type="text" class="form-control" name="task">
            </div>
            <div class="form-group">
                <label for="date">Pasirinkti terminą: </label>
                <input type="datetime-local" step=1 class="form-control" name="date">
            </div>
            
            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <button type="submit" class="btn btn-primary">Sukurti</button>
        </form>
        <br>
        <form action="{{ action('CategoryController@store') }}" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="category">Sukurti kategoriją: </label>
                <input type="text" class="form-control" name="category">
            </div>

            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
            <button type="submit" class="btn btn-primary">Sukurti</button>
        </form>
    </div>
    
    
    
    <div class="col-sm-6">
        <table class="table table-condensed">
            <thead>
              <tr>
                <th></th>
                <th>Kategorija</th>
                <th>Užduotis</th>
                <th>Terminas</th>
                <th>Redaguoti</th>
                <th>Ištrinti</th>
                <th>Atlikai?</th>
              </tr>
            </thead>
            <tbody>
                @foreach($tasks as $task)
                    <tr>
                        <td>
                            @if($task->done == 1)
                                <span style="color: green">ATLIKTA</span>
                            @endif
                        </td>
                        <td>{{$task->category->category}}</td>
                        <td>{{$task->task}}</td>
                        <td>{{$task->date}}</td>
                        <td><a href="{{route('redaguoti', $task->id)}}"><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></a></td>
                        <td>
                            <form action="{{ action('TaskController@delete', $task->id) }}" method="post" enctype="multipart/form-data">
                                <input name="_method" type="hidden" value="DELETE">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                <button type="submit" class="rm-style glyphicon glyphicon-minus" aria-hidden="true" style="color:red"></button>
                            </form>
                        </td>
                        <td>
                            <form action="{{ action('TaskController@done', $task->id) }}" method="post" enctype="multipart/form-data">
                                <input name="done" type="hidden" value="1">
                                <input name="_method" type="hidden" value="PUT">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                <button type="submit" class="rm-style glyphicon glyphicon-plus" aria-hidden="true" style="color:green"></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

