<?php

namespace App\Http\Controllers;
use App\Task;
use App\Category;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;


use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        $categories = Category::all();
        
        return view('pagrindinis', compact('tasks', 'categories'));
    }
    
     public function edit(Request $request, $id)
    {
        $task = Task::find($id);
         
        $categories = Category::all();
        
        $task_date = Carbon::createFromFormat('Y-m-d H:i:s', $task->date)->format('Y-m-d\TH:i:s');
        
        return view('uzduotis', compact('task', 'task_date', 'categories'));
    }
    
    public function store(Request $request)
    { 
        $date1 = Carbon::createFromFormat('Y-m-d\TH:i:s', $request['date'])->toDateTimeString();
        
        $task = [
            'category_id' => $request['category_id'],
            'task' => $request['task'],
            'date' => $date1
        ];
        
        Task::create($task);
        
        Session::flash("create_task", "Nauja užduotis buvo sukurta!");
        
        return redirect()->back();
    }
    
     public function update(Request $request, $id)
    {
        $task = Task::find($id);
        $date1 = Carbon::createFromFormat('Y-m-d\TH:i:s', $request['date'])->toDateTimeString();
         
        $data = [
            'category_id' => $request['category_id'],
            'task' => $request['task'],
            'date' => $date1
        ];
        
        $task->update($data);
        
        return redirect('/');
    }
    
    public function done(Request $request, $id)
    {
        $task = Task::find($id);
        
        $task->update(['done' => $request['done']]);
        
        Session::flash("create_task", "Užduotis atlikta!");
        
        return redirect('/');
    }
    
    public function delete($id)
    {
        $task = Task::find($id);
        
        $task->delete();
        
        Session::flash("delete_task", "Užduotis buvo ištrinta!");
        
        return redirect('/');
    }
}
