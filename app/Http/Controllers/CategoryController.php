<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{

     public function store(Request $request)
    {         
        $category = [
            'category' => $request['category']
        ];
        
        Category::create($category);
        
        return redirect()->back();
    }
}
