<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TaskController@index');
Route::post('/create', 'TaskController@store');
Route::put('/done/{id}', 'TaskController@done');
Route::get('/redaguoti/{id}', 'TaskController@edit')->name('redaguoti');
Route::put('/redaguoti/{id}', 'TaskController@update');
Route::delete('/istrinti/{id}', 'TaskController@delete');

Route::post('/create-category', 'CategoryController@store');


