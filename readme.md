# Do one of the following tasks:

#### *Task A:* create a TODO list app using Laravel

#### *Task B:* create a TODO list app using Laravel and Angular

# Design of the app

- There must be feature to create lists to categorize todo tasks.
- Todo task should contain description and due date with exact time.
- We can mark the todo task as completed.
- We can edit and delete the todo tasks.
- We should get notification if any of todo tasks is overdue.
- Laravel code should be found in `laravel` folder.
- Angular code should be found in `angular` folder. 

# You have *6 hours* to do the task

# Have it done?
Create a private repo on bitbucket, upload your code and grant me (`and2s-dipwork`) read access.